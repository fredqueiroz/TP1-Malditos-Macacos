#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "mem.h"

typedef struct Arquivo{ // Struct para auxiliar na manipulacao dos arquivos temporarios
    FILE *f;
    int pos;    
}Arquivo;

// a_menor_que_b() : Verifica se a string `a` é menor, lexicograficamente, que a string `b`.
//                 : Retorna 1 se `a` menor que `b`, e 0 caso contrario
// Parâmetros:
//     - a: uma cadeia de caracteres de tamanho `len`.
//     - b: uma cadeia de caracteres de tamanho `len`.
//     - len: o tamanho (quantidade de bytes) de `a` e `b`.
int a_menor_que_b(char* a, char* b, int len) {
    int i;
    for(i=0; i<len; i++){
        if(a[i] < b[i])
            return 1;
        else if(a[i] > b[i])
            return 0;
    }
    // Se chegar ate aqui, `a` e `b` sao iguais
    return 0;
}

// SaveFile() : Salva vetor de strings `m` em um arquivo temporario
// Parametros:
//      - nome: nome do arquivo temporario a ser criados.
//      - m: vetor de strings a ser salvo no arquivo `nome`.
//      - num_lines: numero de registros do vetor de strings.
void SaveFile(char *nome, char **m, int num_lines){
    int i;
    FILE *f = fopen(nome, "w"); //Abre arquivo `nome` para leitura
    
    for(i=0; i<num_lines; i++) // Para cada linha do vetor de strings
        fputs(m[i], f);        // Grava a linha no arquivo temporario `nome`
    
    fclose(f); // Fechar arquivo 'nome'
}

// SortStrings() : Ordena vetor de strings `m`.
// Parametros:
//      - m: vetor de strings contendo os registros a serem ordenados.
//      - num_lines: numero de registros do vetor de strings
//      - len: tamanho de cada string (numero de caracteres).
void SortStrings(char **m, int num_lines, int len){
    int i, j;
    char *temp = (char*) malloc((len+2)*sizeof(char)); // vetor AUXILIAR para trocar duas strings do vetor `m` de posicao
    
    for(i=0; i<num_lines-1; i++){ // Compara cada linha do vetor `m` com todas as linhas seguintes a ela
        for(j=i+1; j<num_lines; j++){
            if(!a_menor_que_b(m[i], m[j], len)){ // se a string m[i] for maior que m[j] : troca as duas strings de posicao
                strcpy(temp, m[i]);
                strcpy(m[i], m[j]);
                strcpy(m[j], temp);
            }
        }
    }
    free(temp);
}

// CreateSortedFiles() : Cria arquivos temporarios ordenados indivudualmente a partir do arquivo `input_file`
//                     : Retorna o numero de arquivos temporarios criados
// Parametros:
//      - input_file: o nome do arquivo com os registros de entrada.
//      - memory: o limite de memória a ser usado.
//      - len: tamanho de cada string (numero de caracteres).
int CreateSortedFiles(const char* input_file, unsigned int memory, int *len){
    int num_max_lines, total = 0, num_files = 0, i; // len = string length; num_max_lines = num maximo de linhas que cabem na memoria; num_files = num de arquivos criados
    char novo[10]; // vetor AUXILIAR para nomear arquivos temporários
    
    FILE *input_f = fopen(input_file, "r"); // abre arquivo input_file para leitura
    
    fscanf(input_f, "%d%*c", len); //tamanho de cada string
    
    num_max_lines = floor((memory * 1024.0) / (*len + 10)); // numero maximo de strings de tamanho `len` que podem ser alocadas com `memory` KBs    
    
    //Vetor de strings para armazenar as linhas do arquivo input_file
    char **m;
    m = (char **) mathias_malloc(num_max_lines*sizeof(char *)); // vetor de ponteiros para ponteiros de char
    for(i=0; i<num_max_lines; i++)
        m[i] = (char *) mathias_malloc((*len+2)*sizeof(char)); // vetor de char (string)
        
    while(fgets(m[total], *len+2, input_f) != NULL){ // le uma linha do arquivo input_file
        total++;
        if(feof(input_f)) // verifica se o arquivo input_file chegou ao fim (EOF)
            break;
        
        if(total == num_max_lines){ //Se essa condicao for verdadeira, significa que o vetor de strings esta cheio e precisa ser gravado em um arquivo
            num_files++; 
            sprintf(novo, "T%d.txt", num_files); //nomeia o arquivo temporario
            SortStrings(m, total, *len); // Ordena o vetor de strings
            SaveFile(novo, m, total); // Grava o vetor de strings no arquivo temporario
            total = 0; // volta para a primeira posicao do vetor de strings
        }
    }
    if(total > 0){ // Se sobraram linhas, grava em um novo arquivo
        num_files++;
        sprintf(novo, "T%d.txt", num_files);
        SortStrings(m, total, *len);
        SaveFile(novo, m, total);
    }
    
    for(i=0; i<num_max_lines; i++) //Libera memoria alocada para o vetor de strings
        mathias_free(m[i]);
    mathias_free(m);
    
    fclose(input_f); // Fecha arquivo input_file

    return num_files;
}

// FindSmaller() : Encontra a menor linha dentro dos arquivos temporarios. 
//               : Retorna 1 se foi possivel encontrar uma linha, e 0 caso contrario.
// Parametros:
//      - file: vetor de structs Arquivo.
//      - num_files: numero de arquivos temporarios.
//      - len: tamanho de cada string (numero de caracteres).
//      - smaller: string para retornar a menor linha.

int FindSmaller(Arquivo *file, int num_files, int len, char *smaller){ // 
    int i, idx = -1;
    long int offset = -((len+1)*sizeof(char)); // offset para voltar exatamente uma linha no arquivo
    
    char * buffer_a = (char *) mathias_malloc((len+2)*sizeof(char));
    char * buffer_b = (char *) mathias_malloc((len+2)*sizeof(char));

    for(i=0; i<num_files; i++){
        fgets(buffer_a, len+2, file[i].f); //Verifica se o arquivo chegou ao fim (EOF)
        if(feof(file[i].f)) // Se o arquivo chegou ao fim: passa para o proximo arquivo
            continue;
        else{
            fseek(file[i].f, offset, SEEK_CUR); // Retorna o ponteiro do arquivo para a linha anterior
            if(idx == -1)
                idx = i;
            else{
                fgets(buffer_a, len+2, file[i].f); // Le uma string do arquivo temporario `i`
                fgets(buffer_b, len+2, file[idx].f); // Le um string do arquivo temporario `idx`

                fseek(file[i].f, offset, SEEK_CUR); // Retorna o ponteiro do arquivo `i` para a linha anterior
                fseek(file[idx].f, offset, SEEK_CUR); // Retorna o ponteiro do arquivo `idx` para a linha anterior
                
                if(strcmp(buffer_a, buffer_b) < 0){ // Compara a linha do arquivo `i` com a linha do arquivo `idx`
                    idx = i; // Se a linha do aqruivo `i` for menor que do arquivo `idx` : idx recebe o arquivo com menor linha
                }
            }
        }
    }
    
    mathias_free(buffer_a);
    mathias_free(buffer_b);
    
    if(idx != -1){ // Se foi possivel encontrar a menor linha:
        fgets(smaller, len+2, file[idx].f); // Escreve a linha no vetor AUXILIAR `smaller`
        file[idx].pos++;
        return 1;
    }
    return 0;
}

// MergeSortedFiles() : Realiza a intercalacao dos arquivos temporarios, gravando as linhas ordenadas no arquivo `output_file`
// Parametros:
//      - output_file: o nome do arquivo com os registros de entrada ordenados.
//      - num_files: numero de arquivos temporarios.
//      - len: tamanho de cada string (numero de caracteres).
void MergeSortedFiles(const char *output_file, int num_files, int len){
    char novo[10]; // vetor AUXILIAR para nomear arquivos temporários
    int i;
    FILE *output_f = fopen(output_file, "a"); // abre arquivo output_file para leitura
    
    fprintf(output_f, "%d\n", len); // grava no arquivo `output_file` o tamanho dos registros
    
    Arquivo *file = (Arquivo *) malloc(num_files*sizeof(Arquivo)); // Vetor de struct Arquivo para auxiliar o acesso aos arquivos
    
    for(i=0; i<num_files; i++){ // Abre cada arquivo para leitura
        sprintf(novo, "T%d.txt", i+1);
        file[i].f = fopen(novo, "r");
        file[i].pos = 0;
    }
    
    char *smaller = (char *) malloc((len+2)*sizeof(char)); // string AUXILIAR passada para FindSmaller() para retornar o menor registro
    
    while(FindSmaller(file, num_files, len, smaller) == 1){ // Enquanto for possivel encontrar um registro:
        fputs(smaller, output_f); // grava registro no arquivo `output_file`
    }
    for(i=0; i<num_files; i++){ // Fecha e deleta cada arquivo temporario
        sprintf(novo, "T%d.txt", i+1);
        fclose(file[i].f);
        remove(novo);
    }
    free(file);
    free(smaller);
    fclose(output_f);
}

// external_sort() : Ordena os registros do arquivo `input_file` e grava em `output_file` utilizando menos de `memory` KBs de memoria
// Parâmetros:
//     - input_file: o nome do arquivo com os registros de entrada.
//     - output_file: o nome do arquivo com os registros de entrada ordenados.
//     - memory: o limite de memória a ser usado.
void external_sort(const char* input_file, const char* output_file, unsigned int memory) {
    int num_files, len;
    num_files = CreateSortedFiles(input_file, memory, &len);
    MergeSortedFiles(output_file, num_files, len);
}
